<?php

/**
 * @file
 * uw_ct_homepage_alerts_major.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_ct_homepage_alerts_major_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_sidebar'.
  $field_bases['field_sidebar'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_sidebar',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'text_long',
  );

  return $field_bases;
}
