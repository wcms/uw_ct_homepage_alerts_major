<?php

/**
 * @file
 * uw_ct_homepage_alerts_major.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_homepage_alerts_major_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'homepage_alert_major';
  $context->description = 'Displays the homepage_alert_major view (when there is content)';
  $context->tag = 'homepage';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'homepage_content_type_addons-homepage-alert-major' => array(
          'module' => 'homepage_content_type_addons',
          'delta' => 'homepage-alert-major',
          'region' => 'alert-major',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Displays the homepage_alert_major view (when there is content)');
  t('homepage');
  $export['homepage_alert_major'] = $context;

  return $export;
}
