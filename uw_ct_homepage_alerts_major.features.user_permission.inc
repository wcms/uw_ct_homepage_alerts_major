<?php

/**
 * @file
 * uw_ct_homepage_alerts_major.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_homepage_alerts_major_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create alerts_major content'.
  $permissions['create alerts_major content'] = array(
    'name' => 'create alerts_major content',
    'roles' => array(
      'administrator' => 'administrator',
      'emergency alerter' => 'emergency alerter',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any alerts_major content'.
  $permissions['delete any alerts_major content'] = array(
    'name' => 'delete any alerts_major content',
    'roles' => array(
      'administrator' => 'administrator',
      'emergency alerter' => 'emergency alerter',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own alerts_major content'.
  $permissions['delete own alerts_major content'] = array(
    'name' => 'delete own alerts_major content',
    'roles' => array(
      'administrator' => 'administrator',
      'emergency alerter' => 'emergency alerter',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any alerts_major content'.
  $permissions['edit any alerts_major content'] = array(
    'name' => 'edit any alerts_major content',
    'roles' => array(
      'administrator' => 'administrator',
      'emergency alerter' => 'emergency alerter',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own alerts_major content'.
  $permissions['edit own alerts_major content'] = array(
    'name' => 'edit own alerts_major content',
    'roles' => array(
      'administrator' => 'administrator',
      'emergency alerter' => 'emergency alerter',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter alerts_major revision log entry'.
  $permissions['enter alerts_major revision log entry'] = array(
    'name' => 'enter alerts_major revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'emergency alerter' => 'emergency alerter',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override alerts_major authored by option'.
  $permissions['override alerts_major authored by option'] = array(
    'name' => 'override alerts_major authored by option',
    'roles' => array(
      'administrator' => 'administrator',
      'emergency alerter' => 'emergency alerter',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override alerts_major authored on option'.
  $permissions['override alerts_major authored on option'] = array(
    'name' => 'override alerts_major authored on option',
    'roles' => array(
      'administrator' => 'administrator',
      'emergency alerter' => 'emergency alerter',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override alerts_major promote to front page option'.
  $permissions['override alerts_major promote to front page option'] = array(
    'name' => 'override alerts_major promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override alerts_major published option'.
  $permissions['override alerts_major published option'] = array(
    'name' => 'override alerts_major published option',
    'roles' => array(
      'administrator' => 'administrator',
      'emergency alerter' => 'emergency alerter',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override alerts_major revision option'.
  $permissions['override alerts_major revision option'] = array(
    'name' => 'override alerts_major revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'emergency alerter' => 'emergency alerter',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override alerts_major sticky option'.
  $permissions['override alerts_major sticky option'] = array(
    'name' => 'override alerts_major sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
