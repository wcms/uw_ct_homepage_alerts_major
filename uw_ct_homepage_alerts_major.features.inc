<?php

/**
 * @file
 * uw_ct_homepage_alerts_major.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_homepage_alerts_major_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_homepage_alerts_major_node_info() {
  $items = array(
    'alerts_major' => array(
      'name' => t('Alerts - Major'),
      'base' => 'node_content',
      'description' => t('The alert area will <strong>replace</strong> the homepage content when there is "alert - major" content to share/display (e.g. emergency situation on campus). Users will be able to reach the standard homepage through a special link.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
