<?php

/**
 * @file
 * uw_ct_homepage_alerts_major.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uw_ct_homepage_alerts_major_default_rules_configuration() {
  $items = array();
  $items['rules_alerts_major_posted'] = entity_import('rules_config', '{ "rules_alerts_major_posted" : {
      "LABEL" : "Alerts - Major posted",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_update" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "alerts_major" : "alerts_major" } }
          }
        }
      ],
      "DO" : [ { "redirect" : { "url" : "[site:url]" } } ]
    }
  }');
  return $items;
}
